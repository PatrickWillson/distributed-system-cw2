# COMP3211 Distributed Systems Coursework 2

## Setup instructions
In order to get the integrated client working, a python virtual environment must be created, 
and the necessary modules need to be installed.

First, the repository should be downloaded and uncompressed (from whatever format it was in).  
Then, the following command can be used to create a python virtual environment

```commandline
python3 -m venv /path/to/new/virtual/environment
```

This will create a virtual environment in the specified path.  Note: python3 may not be installed or the command
needed to invoke python3 on your machine, so use the following command instead

```commandline
python -m venv /path/to/new/virtual/environment
```

Next, to activate and enter our virtual python environment, we perform the following command

```commandline
source venv/bin/activate
```

Where venv is the path to the directory where our new virtual environment is set up.

Then, we must install the necessary requirements.  So, you will need to cd into the repository folder, then run 
following command

```commandline
pip install -r requirements.txt
```

This installs the necessary modules required to run the server, using the list of requirements found in the repository.

Finally, we can run the server, which can be done in the top of the repository folder, running the following command

```commandline
python3 client.py & python3 company_to_ticker.py & python3 sector_company.py
```

Of course, if python3 isn't the required command, then using the following command instead

```commandline
python client.py & python company_to_ticker.py & python sector_company.py
```

This will take ~10 seconds to boot up as the database is set up.  Then, you can use localhost to access the service
via the web browser.  You can access the following via the following ports -
- Integrated Client - Port 5000
- Sector to Company API - Port 5001
- Company to Ticker API - Port 8080

Any time a new API call for Sector to Company needs to be called, the web page for the integrated client need to be 
refreshed by going to the root index of the site ('/').
