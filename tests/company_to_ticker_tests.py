from requests import request


# test with company that has a single ticker
def test_single_ticker():
    test_url = "http://127.0.0.1:5000/tickers/Activision Blizzard, Inc"
    test_response = request("GET", test_url).json()
    assert test_response["Response"][0]["Name"] == "Activision Blizzard, Inc"
    assert test_response["Response"][0]["Tickers"] == ["ATVI"]


# test with company that has multiple tickers
def test_multiple_ticker():
    test_url = "http://127.0.0.1:5000/tickers/Barclays PLC"
    test_response = request("GET", test_url).json()
    assert test_response["Response"][0]["Name"] == "Barclays PLC"
    assert test_response["Response"][0]["Tickers"] == ["DFVL", "DFVS", "DLBS", "DTUL", "DTUS", "DTYL", "DTYS", "FLAT", "STPP", "TAPR"]


# test with company that doesn't have a ticker in our database
def test_no_tickers():
    test_url = "http://127.0.0.1:5000/tickers/Aardman"
    test_response = request("GET", test_url).json()
    assert test_response["Response"][0]["Name"] == "Aardman"
    assert test_response["Response"][0]["Tickers"] == []


# test with multiple companies that have single tickers
def test_multiple_single_tickers():
    test_url = "http://127.0.0.1:5000/tickers/Pepsico, Inc.;Plexus Corp.;Zoom Video Communications, Inc."
    test_response = request("GET", test_url).json()

    assert test_response["Response"][0]["Name"] == "Pepsico, Inc."
    assert test_response["Response"][0]["Tickers"] == ["PEP"]

    assert test_response["Response"][1]["Name"] == "Plexus Corp."
    assert test_response["Response"][1]["Tickers"] == ["PLXS"]

    assert test_response["Response"][2]["Name"] == "Zoom Video Communications, Inc."
    assert test_response["Response"][2]["Tickers"] == ["ZM"]


# test with multiple companies that have multiple tickers
def test_multiple_multiple_tickers():
    test_url = "http://127.0.0.1:5000/tickers/Pensare Acquisition Corp.;Tottenham Acquisition I Limited;8i Enterprises Acquisition Corp"
    test_response = request("GET", test_url).json()

    assert test_response["Response"][0]["Name"] == "Pensare Acquisition Corp."
    assert test_response["Response"][0]["Tickers"] == ["WRLS", "WRLSR", "WRLSU", "WRLSW"]

    assert test_response["Response"][1]["Name"] == "Tottenham Acquisition I Limited"
    assert test_response["Response"][1]["Tickers"] == ["TOTA", "TOTAR", "TOTAU", "TOTAW"]

    assert test_response["Response"][2]["Name"] == "8i Enterprises Acquisition Corp"
    assert test_response["Response"][2]["Tickers"] == ["JFK", "JFKKR", "JFKKU", "JFKKW"]


# test with multiple companies that have no tickers
def test_multiple_no_tickers():
    test_url = "http://127.0.0.1:5000/tickers/Dunder Mifflin;YouTube;Blackwell Academy"
    test_response = request("GET", test_url).json()

    assert test_response["Response"][0]["Name"] == "Dunder Mifflin"
    assert test_response["Response"][0]["Tickers"] == []

    assert test_response["Response"][1]["Name"] == "YouTube"
    assert test_response["Response"][1]["Tickers"] == []

    assert test_response["Response"][2]["Name"] == "Blackwell Academy"
    assert test_response["Response"][2]["Tickers"] == []


# test with mix of single, multiple, and no tickers
def test_mix_tickers():
    test_url = "http://127.0.0.1:5000/tickers/Abeona Therapeutics Inc.;Adobe Inc.;National General Holdings Corp;" \
               "Ollie's Bargain Outlet Holdings, Inc.;The Official One Direction Fan Club;" \
               "The People's Union Against Duo Mobile"
    test_response = request("GET", test_url).json()

    assert test_response["Response"][0]["Name"] == "Abeona Therapeutics Inc."
    assert test_response["Response"][0]["Tickers"] == ["ABEO", "ABEOW"]

    assert test_response["Response"][1]["Name"] == "Adobe Inc."
    assert test_response["Response"][1]["Tickers"] == ["ADBE"]

    assert test_response["Response"][2]["Name"] == "National General Holdings Corp"
    assert test_response["Response"][2]["Tickers"] == ["NGHC", "NGHCN", "NGHCO", "NGHCP", "NGHCZ"]

    assert test_response["Response"][3]["Name"] == "Ollie's Bargain Outlet Holdings, Inc."
    assert test_response["Response"][3]["Tickers"] == ["OLLI"]

    assert test_response["Response"][4]["Name"] == "The Official One Direction Fan Club"
    assert test_response["Response"][4]["Tickers"] == []

    assert test_response["Response"][5]["Name"] == "The People's Union Against Duo Mobile"
    assert test_response["Response"][5]["Tickers"] == []
