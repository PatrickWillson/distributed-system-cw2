import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.types import String

#read csv file and connect to database file
df = pd.read_csv('data/sector_company.csv')
engine = create_engine('sqlite:///sector_company.db', echo=False)

#converts pandas dataframe to database table and saves it in the database
db = df.to_sql('Companies', con=engine, if_exists='replace',index=False,dtype={
    "Name": String,
    "Sector": String,
    "Industry": String
    }
)

#prints contents of database table
# print(engine.execute('SELECT * FROM Companies').fetchall())