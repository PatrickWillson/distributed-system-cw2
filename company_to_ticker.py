from sqlalchemy import MetaData, Table, Column, Integer, String, engine
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import sessionmaker
from os import remove, urandom
from flask import Flask
from flask_restful import Api
from flask_restful import Resource
import csv


# if True, get data from company_nasdaq.csv
DB_INIT = True


# set up server, api, and database
app = Flask(__name__)
app.secret_key = urandom(16)
api = Api(app)

db_path = "data/tickers.db"

app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + db_path
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db_engine = engine.create_engine("sqlite:///" + db_path)
Session = sessionmaker(bind=db_engine)
meta = MetaData()


# table schema to hold csv data
tickers = Table(
            "tickers", meta,
            Column("Id", Integer, primary_key=True),
            Column("Name", String),
            Column("Ticker", String)
        )

db = SQLAlchemy(app)

data_path = "data/company_nasdaq.csv"


class CompanyToTicker(Resource):

    def get(self, user_input):
        """
        The HTTP GET method for the ticker api
        :param user_input: the list of companies formatted as a comma-separated list
        :return: a json string containing company names and a list of their NASDAQ tickers
        """
        global tickers
        request = user_input.split(";")
        for i, company in enumerate(request):
            request[i] = company.replace("\'", "&#39;")
        result = {"Response": []}
        session = Session()
        for company in request:
            data = session.query(tickers).filter_by(Name=company).all()
            company_tickers = [row[2] for row in data]
            result["Response"].append({"Name": company.replace("&#39;", "\'"), "Tickers": company_tickers})
        return result


api.add_resource(CompanyToTicker, "/tickers/<string:user_input>")


def db_init():
    """
    If a database file exists under same name, remove it, and create a new one.
    Populate this database with information taken from company_nasdaq.csv
    """
    global tickers
    remove(db_path)
    meta.create_all(db_engine)
    conn = db_engine.connect()
    with open(data_path, "r") as data:
        reader = csv.reader(data, delimiter=",")
        for row in enumerate(reader):
            cmd = tickers.insert().values(
                Id=row[0],
                Name=row[1][1],
                Ticker=row[1][0]
            )
            conn.execute(cmd)
    print("Database write finished.")


def main():
    if DB_INIT:
        db_init()
    app.run(debug=True, host="0.0.0.0", port=8080)


if __name__ == "__main__":
    main()
