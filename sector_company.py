from flask import Flask
from flask_restful import Api, Resource, marshal, fields
import json
from sqlalchemy import create_engine

##API by Patrick Willson 

#initialise app and api
app = Flask(__name__)
api = Api(app)

## gets industry sector from the URL, looks up all the companies in that sector and returns an array of them
class Sector(Resource):

    #get command gets user input from url
    def get(self, user_input):

        #open database
        engine = create_engine('sqlite:///sector_company.db', echo=False)

        #sql statement to return companies within the given sector
        companies = engine.execute('SELECT * FROM Companies WHERE lower(Sector)=:input', input=user_input.lower()).fetchall()

        #marshals database output into json serializable form
        mfields = {"Name":fields.String, "Sector":fields.String, "Industry":fields.String}
        companies_marshal = marshal(companies, mfields, envelope='Companies')

        return companies_marshal

#api instantiates class and acquires user input from url
api.add_resource(Sector, '/sector/<string:user_input>')

#run web service app on port 5001
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5001, debug=True)
