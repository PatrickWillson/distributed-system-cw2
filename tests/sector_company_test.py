import requests
import json

#test that API only returns companies of the inputted sector
def test_sector():
    tech = requests.get('http://localhost:5001/sector/technology').json()
    misc = requests.get('http://localhost:5001/sector/miscellaneous').json()
    assert tech["Companies"][0]["Sector"] == "Technology"
    assert misc["Companies"][0]["Sector"] == "Miscellaneous"

def test_name():
    companies = requests.get('http://localhost:5001/sector/technology').json()
    assert companies['Companies'][0]['Name'] == "21Vianet Group, Inc."

def test_inavlid_input():
    companies = requests.get('http://localhost:5001/sector/invalid_input').json()
    assert companies["Companies"] == []

if __name__ == '__main__':
    test_sector()
    test_name()
    test_inavlid_input()

    print("All Tests Passed")