from random import randint
import requests
from time import time
from statistics import mean, stdev

#array of all valid sectors
sectors = ['Health Care', 'Finance', 'Consumer Services', 'Technology', 'Capital Goods', 'Energy', 'Miscellaneous', 'Public Utilities', 'Basic Industries', 'Transportation', 'Consumer Non-Durables', 'Consumer Durables']
times = []

#repeats n (5) times
for n in range(5):
    #measures invocation time
    sector_index = randint(0, len(sectors)-1)
    start = time()
    requests.get('http://localhost:5001/sector/'+sectors[sector_index]).json()
    end = time()
    times.append(end-start)

#calculate mean and standard deviation
mean_average = mean(times)
standard_deviation = stdev(times)

#output results
print("times:", times)
print("average:", mean_average)
print("standard devation:", standard_deviation)