from flask import Flask, render_template, request, flash, redirect
from flask.wrappers import Response
from flask_wtf import FlaskForm
from requests.sessions import session
from sqlalchemy.sql.expression import label
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired, Optional
import yfinance as yf
import yfinance.shared as shared
from datetime import datetime
from dateutil.relativedelta import relativedelta
import io
import json
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import os
import requests
import base64

#initialise app and set secret key
app = Flask(__name__)
app.secret_key = os.urandom(16)

#declaring global dictionary to store users selections
selections = {
    'companies': None,
    'company names': None,
    'ticker': None
}

#creating forms
class SectorForm(FlaskForm):
    sector = StringField('sector')
    company = SelectField('company', coerce=str, validators=[Optional()])

#homepage
@app.route("/", methods=['GET', 'POST'])
def home():
    global selections
    #instantiate form
    form = SectorForm()
    graphs = None
    if selections['company names'] == None:
        form.company.choices = []
        form.sector.validators = [DataRequired()]
    else:
        form.company.choices = selections['company names']
        form.sector.validators = [Optional()]
    if form.validate_on_submit():
        ## 1st API
        #get form data then use first web service api to get the companies from the given sector
        sector = request.form['sector']
        if sector != "":
            try:
                #request data from first api
                companies = requests.get('http://localhost:5001/sector/'+sector).json()
                #if no data returned
                if len(companies["Companies"]) == 0:
                    flash("This is not a valid sector. Try searching for 'Technology'")
                    selections['companies'] = None
                else:
                    #updates selections dictionary
                    selections['companies'] = companies
            except:
                flash("Error: Web service is offline")
                selections['companies'] = None

        ## 2nd API
        if selections['companies'] != None:
            company_json = selections["companies"]
            if selections['company names'] == None:
                selections['company names'] = [company_json["Companies"][i]["Name"] for i in range(0, len(company_json["Companies"]))]
                return redirect("/")
            if form.company.data:
                selections['ticker'] = form.company.data
                ticker = requests.request("GET", "http://localhost:8080/tickers/" + str(selections["ticker"])).json()
                if len(ticker["Response"][0]["Tickers"]) > 0:
                    selections["ticker"] = [ticker["Response"][0]["Tickers"][0]]
                else:
                    selections["ticker"] = []
            ## 3rd API (Yahoo Finance API)
            if selections['ticker'] != None:
                graphs = []
                for t in selections['ticker']:
                    #connect to API and historical and current stock prices
                    data = yf.download(t, start=datetime.now()-relativedelta(years=1), end=datetime.now())
                    if len(shared._ERRORS.keys()) > 0:
                        flash("Error: " + str(shared._ERRORS.keys()) + " cannot be retrieved from yahoo finance")
                        continue

                    #select colour for graph based on the stock's performance over the time period
                    colour = "gray"
                    if data['Close'][0] > data['Close'][-1]:
                        colour = 'red'
                    elif data['Close'][0] < data['Close'][-1]:
                        colour = 'green'

                    #generate graph
                    figure = Figure()
                    graph = figure.add_subplot(1,1,1)
                    graph.plot(data.index, data['Close'], color=colour)
                    graph.set_title(t)
                    graph.set_xlabel("Date")
                    graph.set_ylabel("Share Price ($)")

                    #convert graph to image
                    image = io.BytesIO()
                    FigureCanvas(figure).print_png(image)
                    graph_data = image.getvalue()

                    #encode and format graph so it can be represented in HTML
                    graph_data = base64.b64encode(graph_data)
                    graph_data = graph_data.decode()
                    graph_image = "data:image/png;base64,{}".format(graph_data)
                    graphs.append(graph_image)
    return render_template('index.html', form=form, graphs=graphs, companies=selections['company names'])

#run app on part 5000
if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5000)