from datetime import datetime
from random import randint
from time import perf_counter

from dateutil.relativedelta import relativedelta
from requests import request
from statistics import mean, stdev
import csv
import yfinance as yf


PRECISION = 4


def main():
    tests = []
    with open("../data/company_nasdaq.csv") as data:
        reader = csv.reader(data, delimiter=",")
        for i in range(0, 5):
            next_test = []
            for j in range(0, randint(1, 5)):
                next_test.append(reader.__next__()[0])
            tests.append(next_test)
    test_results = []
    for test in tests:
        start = perf_counter()
        data = yf.download(test, start=datetime.now() - relativedelta(years=1), end=datetime.now())
        end = perf_counter()
        test_results.append(round(end - start, PRECISION))
    mean_time = round(mean(test_results), PRECISION)
    stdev_time = round(stdev(test_results), PRECISION)
    with open("test_results.txt", "w") as output:
        for i in range(0, len(tests)):
            output.write("Test " + str(i) + " - " + str(test_results[i]) + " seconds\n")
        output.write("Mean Average: " + str(mean_time) + " seconds\n")
        output.write("Standard Deviation: " + str(stdev_time))
    print("Mean: " + str(mean_time) + " Stdev: " + str(stdev_time))


if __name__ == "__main__":
    main()
