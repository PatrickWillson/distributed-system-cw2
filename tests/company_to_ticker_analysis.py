from time import perf_counter
from requests import request
from statistics import mean, stdev


PRECISION = 4


def main():
    tests = ["Barclays PLC", "Aardman",
             "Pensare Acquisition Corp.;Tottenham Acquisition I Limited;8i Enterprises Acquisition Corp",
             "Pepsico, Inc.;Plexus Corp.;Zoom Video Communications, Inc.",
             "Activision Blizzard, Inc"]
    test_results = []
    for test in tests:
        test_url = "http://localhost:8080/tickers/" + test
        start = perf_counter()
        test_response = request("GET", test_url)
        end = perf_counter()
        test_results.append(round(end - start, PRECISION))
    mean_time = round(mean(test_results), PRECISION)
    stdev_time = round(stdev(test_results), PRECISION)
    with open("test_results.txt", "w") as output:
        for i in range(0, len(tests)):
            output.write("Test " + str(i) + " - " + str(test_results[i]) + " seconds\n")
        output.write("Mean Average: " + str(mean_time) + " seconds\n")
        output.write("Standard Deviation: " + str(stdev_time))
    print("Mean: " + str(mean_time) + " Stdev: " + str(stdev_time))


if __name__ == "__main__":
    main()
